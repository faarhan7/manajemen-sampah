<?php

namespace App\Http\Controllers;

use App\Models\Trash;
use App\Models\Category;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $categories_count = Category::count();
        $trashes_count = Trash::count();
        return view('dashboard.index', [
            'categories_count' => $categories_count,
            'trashes_count' => $trashes_count
        ]);
    }
}
