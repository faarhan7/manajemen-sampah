<?php

namespace App\Http\Controllers;

use App\Models\Trash;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrashController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $item = Trash::with('category')->get();
        // $item = Trash::withTrashed()->get();

        return view('trashes.index', [
            'trashes' => $item
        ]);

    }

    public function archieve()
    {
        // $item = Trash::with('category')->get();
        $item = Trash::onlyTrashed()->get();

        return view('trashes.archieve', [
            'trashes' => $item
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('trashes.create',['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $new_trash = new \App\Models\Trash;
        // $new_trash->category_id = $request->get('category_id');
        // $new_trash->nama_sampah = $request->get('nama_sampah');
        // $new_trash->deskripsi = $request->get('deskripsi');
        $validated = $request->validate([
            'category_id' => 'required',
            'nama_sampah' => ['required', 'min:3', 'max:50'],
            'deskripsi' => ['required', 'min:3', 'max:255']
        ]);
        // dd($validated);
        Trash::create($validated);
        // $new_trash->save();

        return redirect('/trashes')->with('success', 'Data sampah berhasil dimasukkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Trash $trash)
    {
        $categories = Category::all();

        return view('trashes.show', [
            'trash'=>$trash,
            'categories'=>$categories
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Trash $trash)
    {
        $categories = Category::all();
        return view('trashes.edit', ['trash'=>$trash, 'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trash $trash)
    {
        // dd($request);
        $validated = $request->validate([
            'category_id' => 'required',
            'nama_sampah' => ['required', 'min:3', 'max:50'],
            'deskripsi' => ['required', 'min:3', 'max:255']
        ]);

        $trash->update($validated);
        
        return redirect('/trashes')->with('message', 'Data sampah berhasil dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trash $trash)
    {
        $trash->delete();

        return redirect('/trashes')->with('danger', 'Data sampah berhasil dihapus');

    }

    public function restore($id)
    {
        $trash = Trash::onlyTrashed()->findOrFail($id);
        $trash-> restore();
        // $trash->delete();

        return redirect('/trashes')->with('success', 'Data sampah arsip berhasil dipulihkan');


    }

    public function forcedelete($id)
    {
        $trash = Trash::onlyTrashed()->findOrFail($id);
        $trash-> forceDelete();
        // $trash->delete();

        return redirect('/trashes')->with('danger', 'Data sampah arsip berhasil dihapus permanen');

    }

    public function search(Request $request)
    {
        $output="";

        $loweredSearchedText = strtolower($request->search);

        $trashes=Trash::where(DB::raw('lower(nama_sampah)'), 'like','%'.$loweredSearchedText.'%')->get();
        $trashed=Trash::where(DB::raw('lower(nama_sampah)'), 'like','%'.$loweredSearchedText.'%')->onlyTrashed()->get();
        // dd($trashes);
        // dd($request->search);
        $no=1;
        foreach($trashes as $trash)
        {
            $output.=
            '<tr>
                <td>'. $no.'</td>
                <td>'. $trash->category->nama_kategori.'</td>
                <td>'. $trash->nama_sampah.'</td>
                <td>'. $trash->deskripsi.'</td>
                <td>
                <div class="btn-group">'.'
                    <a href="/trashes/'.$trash->id.'" class="btn btn-sm btn-primary mr-1">'.'lihat</a>
                    '.'
                    <a href="/trashes/'.$trash->id.'/edit" class="btn btn-sm btn-success mr-1">'.'Edit</a>
                    '.'
                    <a href="/trashes/'.$trash->id.'/delete" onClick="return confirm('.'`Yakin ingin menghapus?`'.')" class="btn btn-sm btn-danger mr-1">'.'delete</a>
                    '.'
                </td></tr>';
            $no++;
        }
        // foreach($trashed as $trash)
        // {
        //     $output.=
        //     '<tr>
        //         <td>'. $no.'</td>
        //         <td>'. $trash->category->nama_kategori.'</td>
        //         <td>'. $trash->nama_sampah.'</td>
        //         <td>'. $trash->deskripsi.'</td>
        //         <td class="text-center">
        //         <div class="btn-group">'.'
        //             <a href="/trashes/'.$trash->id.'/restore" class="btn btn-sm btn-success mr-1">'.'Restore</a>
        //             '.'
        //             <a href="/trashes/'.$trash->id.'/forcedelete" class="btn btn-sm btn-danger mr-1">'.'Hapus Permanen</a>
        //             '.'
        //         </td></tr>';
        //     $no++;
        // }
        return response($output);
    }
}
