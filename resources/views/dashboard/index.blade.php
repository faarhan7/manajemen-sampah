@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <h1 class="mt-4">Dashboard</h1>
	<ol class="breadcrumb mb-4">
	    <li class="breadcrumb-item active">Dashboard</li>
	</ol>
  <div class="row">
		<div class="col">
			<h1>Selamat datang di Dashboard {{auth()->user()->name}}</h1>
		</div>
  </div>
  <div class="row">
    <div class="col-xl-6 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs mb-1">
                        Jumlah Kategori
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $categories_count }}</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-chart-area fa-2x text-gray-300"></i>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs mb-1">
                        Jumlah Data Sampah
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $trashes_count }}</div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-table fa-2x text-gray-300"></i>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection