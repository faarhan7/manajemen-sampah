@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Data Sampah</h1>
	<ol class="breadcrumb mb-4">
	    <li class="breadcrumb-item active">Dashboard / Data Sampah</li>
	</ol>
    <x-flash-message></x-flash-message>

		<a href="/trashes/create" class="btn btn-primary">Masukkan Data Baru</a><br><br>
        <a href="/trashes/archieve">Lihat Arsip Data</a><br>
        <div class="search">
            <input type="search" name="search" id="search" placeholder="Cari disini" class="form-control mb-3">
        </div>
        <div class="table-responsive">
            <table class="table " id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>Kategori Jenis Sampah</th>
                        <th>Nama Sampah</th>
                        <th>Deskripsi</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody class="alldata">
                    @php $no=1 @endphp
                	@foreach($trashes as $trash)
                    <tr>
                    	<td><?= $no ?></td>
                        <td>{{ $trash->category->nama_kategori }}</td>
                        <td>{{ $trash->nama_sampah }}</td>
                        <td>{{ $trash->deskripsi }}</td>
                        <td class="text-center">
                        <div class="btn-group">
                        <a href="/trashes/{{ $trash->id }}" class="btn btn-sm btn-primary mr-1">Lihat</a><br>
                        <a href="/trashes/{{ $trash->id }}/edit" class="btn btn-sm btn-success mr-1">Edit</a><br>
                        <a href="/trashes/{{ $trash->id }}/delete" class="btn btn-sm btn-danger mr-1" onClick="return confirm('Yakin ingin menghapus?')">Delete</a><br>

                        {{-- <a href="posts/{{$trash->id}}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?"></a> --}}

                        {{-- <form method="POST" action="/trashes/{{ $trash->id }}" >
                            @csrf
                            @method('DELETE')
                            <div class="control">
                            <button type="submit" class="btn btn-sm btn-danger" onClick="return confirm('Yakin ingin menghapus?')">Delete</button>
                            </div>
                        </form> --}}
                        </div>
                    	</td>
                    @php $no++; @endphp
                        
                    </tr>
                    @endforeach
                    

                </tbody>
                <tbody id="Content" class="searchdata">

                </tbody>
            </table>
        </div>
        
</div>

@endsection