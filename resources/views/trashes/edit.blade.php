@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <div class="mt-4">Data Sampah</div>
  <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard / Data Sampah</li>
  </ol>
  <div class="card">
    <div class="card-body">
      <form action="/trashes/{{$trash->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="">Nama Kategori</label>
          <select name="category_id" id="" class="form-control">
            @foreach($categories as $category)
            <option value="{{ $category->id }}" @if($category->id===$trash->category_id) selected @endif>{{$category->nama_kategori}}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <label for="">Nama Sampah</label>
          <input type="text" class="form-control" name="nama_sampah" value="{{ $trash->nama_sampah }}">
          @error('nama_sampah')
              <p class="text-danger text-xs mt-1">
                {{$message}}
              </p>
          @enderror
        </div>
        <div class="form-group">
          <label for="">Deskripsi</label>
          <textarea name="deskripsi" class="form-control">{{ $trash->deskripsi }}</textarea>
          @error('deskripsi')
              <p class="text-danger text-xs">
                {{$message}}
              </p>
          @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
    </div>
  </div>
</div>
@endsection