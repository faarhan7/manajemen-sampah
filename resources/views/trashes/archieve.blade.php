@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Arsip Data Sampah</h1>
	<ol class="breadcrumb mb-4">
	    <li class="breadcrumb-item active">Dashboard / Data Sampah</li>
	</ol>
    <x-flash-message></x-flash-message>

        {{-- <div class="search">
            <input type="search" name="search" id="search" placeholder="Cari disini" class="form-control mb-3">
        </div> --}}
        <div class="table-responsive">
            <table class="table " id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>Kategori Jenis Sampah</th>
                        <th>Nama Sampah</th>
                        <th>Deskripsi</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody class="alldata">
                    @php $no=1 @endphp
                	@foreach($trashes as $trash)
                    <tr>
                    	<td><?= $no ?></td>
                        <td>{{ $trash->category->nama_kategori }}</td>
                        <td>{{ $trash->nama_sampah }}</td>
                        <td>{{ $trash->deskripsi }}</td>
                        <td class="text-center">
                        <div class="btn-group">
                        <a href="/trashes/{{ $trash->id }}/restore" class="btn btn-sm btn-success mr-1" onClick="return confirm('Yakin ingin mengembalikan data ini?')">Restore</a><br>
                        <a href="/trashes/{{ $trash->id }}/forcedelete" class="btn btn-sm btn-danger mr-1" onClick="return confirm('Yakin ingin menghapus?')">Hapus Permanen</a><br>
                        
                        {{-- <form method="POST" action="/trashes/{{$trash->id}}/restore" >
                            @csrf
                            <div class="control">
                            <button type="submit" class="btn btn-sm btn-success mr-1" onClick="return confirm('Yakin ingin mengembalikan data ini?')">Restore</button>
                            </div>
                        </form> --}}

                        {{-- <form method="POST" action="/trashes/{{$trash->id}}/forcedelete" >
                            @csrf
                            <div class="control">
                            <button type="submit" class="btn btn-sm btn-danger mr-1" onClick="return confirm('Yakin ingin menghapus?')">Hapus Permanen</button>
                            </div>
                        </form> --}}
                        </div>
                    	</td>
                    @php $no++; @endphp
                        
                    </tr>
                    @endforeach
                    

                </tbody>
                <tbody id="Content" class="searchdata">

                </tbody>
            </table>
        </div>
        
</div>

@endsection