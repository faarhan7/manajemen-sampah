@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <h1 class="mt-4">Data Sampah</h1>
  <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard / Data Sampah</li>
  </ol>

  <h4>{{ $trash->nama_sampah }}</h4>
	<p>Kategori : {{ $trash->category->nama_kategori }}</p>
  <p>Deskripsi : {{ $trash->deskripsi }}</p>


</div>
@endsection