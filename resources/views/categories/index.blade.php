@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
	<h1 class="mt-4">Kategori</h1>
	<ol class="breadcrumb mb-4">
	    <li class="breadcrumb-item active">Dashboard / Kategori</li>
	</ol>
    <x-flash-message />

		<a href="/categories/create" class="btn btn-primary">Buat Kategori Baru</a><br><br>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>Kategori</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php $no=1 @endphp
                	@foreach($categories as $category)
                    <tr>
                        <td><?= $no ?></td>
                        <td>{{ $category->nama_kategori }}</td>
                    	<td class="text-center">
                        <div class="btn-group">
                            <a href="/categories/{{ $category->id }}/edit" class="btn btn-sm btn-success mr-1">Edit</a><br>
                            <form method="POST" action="/categories/{{ $category->id }}" >
                                @csrf
                                @method('DELETE')
                                <div class="control">
                                <button type="submit" class="btn btn-sm btn-danger" onClick="return confirm('Yakin ingin menghapus?')">Delete</button>
                                </div>
                            </form>
                        </div>
                    	</td>
                    @php $no++; @endphp
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</div>
@endsection