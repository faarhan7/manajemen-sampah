@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
  <div class="mt-4">Kategori</div>
  <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard / Kategori</li>
  </ol>
  <div class="card">
    <div class="card-body">
      <form action="/categories/{{$category->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="">Nama Kategori</label>
          <input type="text" class="form-control" name="nama_kategori" value="{{ $category->nama_kategori }}">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
    </div>
  </div>
</div>
@endsection