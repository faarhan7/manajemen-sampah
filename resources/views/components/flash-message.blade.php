@if(session()->has('message'))
<div class="container mt-2">
  <div x-data="{show: true}" x-init="setTimeout(()=>show = false, 3000)" x-show="show" class="alert alert-warning alert-dismissible fade show text-center">
    <p>
      {{-- {{session('message')}} --}}
      {{ session()->get('message') }}
    </p>
  </div>
</div>
@endif

@if(session()->has('danger'))
<div class="container mt-2">
  <div x-data="{show: true}" x-init="setTimeout(()=>show = false, 3000)" x-show="show" class="alert alert-danger alert-dismissible fade show text-center">
    <p>
      {{-- {{session('message')}} --}}
      {{ session()->get('danger') }}
    </p>
  </div>
</div>
@endif

@if(session()->has('success'))
<div class="container mt-2">
  <div x-data="{show: true}" x-init="setTimeout(()=>show = false, 3000)" x-show="show" class="alert alert-success alert-dismissible fade show text-center">
    <p>
      {{-- {{session('message')}} --}}
      {{ session()->get('success') }}
    </p>
  </div>
</div>
@endif