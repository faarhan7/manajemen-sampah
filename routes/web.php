<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TrashController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Dashboard
Route::get('/', [DashboardController::class, 'index'])->middleware('auth');

// Category
Route::get('/categories', [CategoryController::class, 'index']);
Route::get('/categories/create', [CategoryController::class, 'create']);
Route::post('/categories', [CategoryController::class, 'store']);
Route::get('/categories/{category}/edit', [CategoryController::class, 'edit']);
Route::put('/categories/{category}', [CategoryController::class, 'update']);
Route::delete('/categories/{category}', [CategoryController::class, 'destroy']);

// Trash
Route::get('/trashes', [TrashController::class, 'index']);
Route::get('/trashes/archieve', [TrashController::class, 'archieve']);
Route::get('/trashes/create', [TrashController::class, 'create']);
Route::post('/trashes', [TrashController::class, 'store']);
Route::get('/trashes/{trash}/edit', [TrashController::class, 'edit']);
Route::put('/trashes/{trash}', [TrashController::class, 'update']);
Route::get('/trashes/{trash}/delete', [TrashController::class, 'destroy']);

// Route::post('/trashes/{trash}/restore', [TrashController::class, 'restore']);
Route::get('/trashes/{trash}/restore', [TrashController::class, 'restore']);
// Route::post('/trashes/{trash}/forcedelete', [TrashController::class, 'forcedelete']);
Route::get('/trashes/{trash}/forcedelete', [TrashController::class, 'forcedelete']);

Route::get('/trashes/{trash}', [TrashController::class, 'show']);

Route::get('/search', [TrashController::class, 'search']);




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
